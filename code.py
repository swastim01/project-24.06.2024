import requests as rq

import random

DEBUG = False

class wordle:
    words = [word.strip() for word in open("words_wordle.txt")]
    mm_url = "https://we6.talentsprint.com/wordle/game/"
    register_url = mm_url + "register"
    creat_url = mm_url + "create"
    guess_url = mm_url + "guess"

    def __init__(self, name: str):
        def is_unique(w: str) -> bool:
            return len(w) == len(set(w))

        self.session = rq.session()
        register_dict = {"mode": "wordle", "name": name}
        reg_resp = self.session.post(wordle.register_url, json=register_dict)
        self.me = reg_resp.json()['id']
        creat_dict = {"id": self.me, "overwrite": True}
        self.session.post(wordle.creat_url, json=creat_dict)

        self.choices = [w for w in wordle.words[:] if is_unique(w)]
        random.shuffle(self.choices)

     
    def play(self: Self) -> str:
        def post(choice: str) -> tuple[int, bool]:
        guess = {"id": self.me, "guess": choice}
        response = self.session.post(MMBot.guess_url, json=guess)
        rj = response.json()
        right = int(rj["feedback"])
        status = "win" in rj["message"]
            return right, status

        max_attempts = 6
        attempts = 0

        choice = random.choice(self.choices)
        self.choices.remove(choice)
        feedback, won = post(choice)
        tries = [f'{choice}:{feedback}']

        while not won:
            if DEBUG:
                print(choice, feedback, self.choices[:10])
            self.update(choice, feedback)
            choice = random.choice(self.choices)
            self.choices.remove(choice)
            feedback, won = post(choice)
            tries.append(f'{choice}:{feedback}')
        print("Secret is", choice, "found in", len(tries), "attempts")
        print("Route is:", " => ".join(tries))

    def update(self, choice: str, feedback: str):
        def is_valid(word: str) -> bool:
            for i, ch in enumerate(feedback):
                if ch == 'G' and word[i] != choice[i]:
                    return False
                if ch == 'Y' and (word[i] == choice[i] or choice[i] not in word):
                    return False
                if ch == 'R' and choice[i] in word:
                    return False
            return True
        
        self.choices = [w for w in self.choices if is_valid(w)]

game = wordle("Bot: 7, 44, 81, 118")
game.play()
