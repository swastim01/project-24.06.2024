red, green, yellow = 'R', 'G', 'Y'
won = 'GGGGG'

def green_pos(feedback: str) -> list[int]:
	return [pos for pos, character in enumerate(feedback) if character == green]

def green_matches(sample: str, feedback: str, chance:str) -> bool:
	return all([sample[i] == chance[i] for i in green_pos(feedback)])

def red_matches(sample: str, feedback: str, chance: str) -> bool:
            red_match = ''.join(sam for (sam, feed) in zip(sample, feedback) if feed == red)
            return all([i not in chance for i in red_match])

def yellow_matches(sample: str, feedback: str, chance: str) -> bool:
            yellow_match = ''.join(sam for (sam, feed) in zip(sample, feedback) if feed == yellow)
            return all([i in possible for i in yellow_match])


if green in self.feedback:
	self.choices = [_ for _ in self.choices if green_matches(choice, self.feedback, _)]
 
if red in self.feedback:
	self.choices = [_ for _ in self.choices if red_matches(choice, self.feedback, _)]
 
 if yellow in self.feedback:
	self.choices = [_ for _ in self.choices if yellow_matches(choice, self.feedback, _)]

